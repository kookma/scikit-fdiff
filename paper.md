---
title: 'scikit-finite-diff, a new tool for PDE solving'
tags:
  - python
  - pde
  - physical modelling
authors:
  - name: Nicolas Cellier
    orcid: 0000-0002-3759-3546	
    affiliation: 1
  - name: Christian Ruyer-Quil
    affiliation: 1
affiliations:
 - name: Université Savoie Mont-Blanc
   index: 1
date: 18 March 2019
bibliography: paper.bib
---

Scikit-FDiff is a new tool, written in pure Python, that
focus on reducing the time between the development of
the mathematical model and the numerical solving.

It allows an easy and automatic finite difference
discretization, thanks to a symbolic processing (using the sympy library 
[@Meurer:2017]) that can deal with systems of multi-dimensional partial
differential equation with complex boundary conditions.

Using finite differences and the method of lines,
it allows the transformation of the original PDE into an ODE, providing a
fast computation of the temporal evolution vector and the Jacobian
matrix. The later is pre-computed in a symbolic way
and is sparse by nature. An efficient vectorization allow to build the
numerical system a way to facilitate the numerical solver work, even for
complex multi-dimensional coupled cases. It can be evaluated with
as few computational resources as possible, allowing
the use of implicit and explicit solvers at a reasonable cost.

It demark itself from other competitors as the FEniCS Project,
Clawpack or the Dedalus Project by its simplicity. While being able
to solve real-world problem, scikit-fdiff use a simple method (the
finite-differences) which allow a writing which is really close to
the mathematic model, and will not be as suited as other tools for
building specialized solvers.

Classic ODE solvers have been implemented (or made available
from dedicated python libraries), including backward and forward
Euler scheme, Crank-Nicolson, explicit Runge-Kutta. More
complex ones, like improved Rosenbrock-Wanner schemes [@Rang:2015]
up to the 6th order, are also available. The time-step is managed
by a built-in error computation, which ensures the accuracy
of the solution.

The main goal of the software is to minimize the time spent
writing numerical solvers to focus on model development
and data analysis. Scikit-Fdiff is then able to solve
toy cases in a few line of code as well as complex models.
Extra tools are available, such as data saving during the
simulation, real-time plotting and post-processing.

It has been validated with the shallow-water equation on dam-breaks [@Leveque:2002]
and the steady-lake case. It has also been applied to heated
falling-films [@Cellier:2018], droplet spread and simple moisture flow in porous
medium.